import java.util.Scanner;

public class Calculator {

    /***
     * Declare class const and class var
     */
    private static final String line = "========================================="; // line for display decoration
    private static int closeApp = 0; // var for looping condition
    private static Scanner scanner = new Scanner(System.in); // Scanner declaration
    private static final float phi = (float) 3.14; // const for phi 22/7 (3.14)

    /***
     * Create method to clear the console
     */
    public static void clearScreen() {
        /*
         * make the cursor move to the first line and column of the screen and then
         * clear console from the cursor to the end of the screen
         */
        System.out.print("\033[H\033[2J");
        System.out.flush(); // flush the console and make it ready to rewrite
    }

    /***
     * Create method to display the main menu
     */
    private static void mainMenu() {
        clearScreen(); // clear console
        System.out.println(line);
        System.out.println("Kalkulator Penghitung Luas dan Volume"); // tittle
        System.out.println(line);
        System.out.println("Menu");
        System.out.println("1. Hitung luas bidang datar");
        System.out.println("2. Hitung volume bangun ruang");
        System.out.println("0. Tutup aplikasi");
        System.out.println(line);
    }

    /***
     * Creating a menu to display when user select 'Hitung luas bidang datar'
     */
    private static void menuBidang() {
        clearScreen(); // clear console
        System.out.println(line);
        System.out.println("Pilih luas bidang datar yang akan di hitung"); // tittle
        System.out.println(line);
        System.out.println("1. Persegi");
        System.out.println("2. Lingkaran");
        System.out.println("3. Segitiga");
        System.out.println("4. Persegi panjang");
        System.out.println("0. Kembali ke menu sebelumnya");
        System.out.println(line);
    }

    /***
     * Create a menu to display when user select 'Hitung volume bangun ruang'
     */
    private static void menuBangunRuang() {
        clearScreen(); // clear console
        System.out.println(line);
        System.out.println("Pilih volume bangun ruang yang akan di hitung"); // tittle
        System.out.println(line);
        System.out.println("1. Kubus");
        System.out.println("2. Balok");
        System.out.println("3. Tabung");
        System.out.println("0. Kembali ke menu sebelumnya");
        System.out.println(line);
    }

    /***
     * Create menus to display when user select one of the type of 'bidang datar'
     * 
     * @param input
     */
    private static void luasBidang(int input) {
        switch (input) {
            /***
             * Create case to display when 'Persegi' selected
             */
            case 1:
                clearScreen(); // clear console
                System.out.println(line);
                System.out.println("Hitung luas persegi"); // tittle
                System.out.println(line);
                System.out.print("Masukan sisi (cm):");
                float sisi = scanner.nextFloat(); // input to entry the 'sisi' value
                System.out.println(line);
                System.out.println(""); // make empty row
                System.out.println("Luas persegi dengan sisi " + sisi + "cm = " + luasPersegi(sisi) + "cm^2"); // result
                System.out.println(""); // make empty row
                // post-calculation menu
                System.out.println("1. Kembali ke menu utama");
                System.out.println("0. Tutup Aplikasi");
                System.out.print("Input : ");
                int next1 = scanner.nextInt();
                switch (next1) {
                    case 1:
                        mainMenu(); // redirect to main menu
                        break;
                    case 0:
                        closeApp = 1; // make a condition to stop the loop
                        break;
                }
                System.out.println(line);
                break;
            /***
             * Create case to display when 'Lingkaran' selected
             */
            case 2:
                clearScreen(); // clear console
                System.out.println(line);
                System.out.println("Hitung luas lingkaran"); // tittle
                System.out.println(line);
                System.out.print("Masukan jari-jari (cm):");
                float jarijari = scanner.nextFloat(); // input to entry 'jari-jari' value
                System.out.println(line);
                System.out.println(""); // make empty row
                System.out.println(
                        "Luas lingkaran dengan jari-jari " + jarijari + "cm = " + luasLingkaran(jarijari) + "cm^2"); // result
                System.out.println(""); // make empty row
                // post-calculation menu
                System.out.println("1. Kembali ke menu utama");
                System.out.println("0. Tutup Aplikasi");
                System.out.print("Input : "); // input of post-calculation menu
                int next2 = scanner.nextInt();
                switch (next2) {
                    case 1:
                        mainMenu(); // redirect to main menu
                        break;
                    case 0:
                        closeApp = 1; // make a condition to stop the loop
                        break;
                }
                System.out.println(line);
                break;
            /***
             * Create case to display when 'Segitiga' selected
             */
            case 3:
                clearScreen(); // clear console
                System.out.println(line);
                System.out.println("Hitung luas segitiga"); // tittle
                System.out.println(line);
                System.out.print("Masukan alas (cm): ");
                float alas = scanner.nextFloat(); // input to entry 'alas' value
                System.out.print("Masukan tinggi (cm): ");
                float tinggi = scanner.nextFloat(); // input to entry 'tinggi' value
                System.out.println(line);
                System.out.println(""); // make empty row
                System.out.println("Luas segitiga dengan alas " + alas + "cm dan tinggi " + tinggi + "cm = "
                        + luasSegitiga(alas, tinggi) + "cm^2"); // result
                System.out.println(""); // make empty row
                // post-calculation menu
                System.out.println("1. Kembali ke menu utama");
                System.out.println("0. Tutup Aplikasi");
                System.out.print("Input : ");
                int next3 = scanner.nextInt(); // input of post-calculation menu
                switch (next3) {
                    case 1:
                        mainMenu(); // redirect to main menu
                        break;
                    case 0:
                        closeApp = 1; // make a condition to stop the loop
                        break;
                }
                System.out.println(line);
                break;
            /***
             * Create case to display when 'Persegi panjang' selected
             */
            case 4:
                clearScreen(); // clear console
                System.out.println(line);
                System.out.println("Hitung luas persegi panjang"); // tittle
                System.out.println(line);
                System.out.print("Masukan panjang (cm): ");
                float panjang = scanner.nextFloat(); // input to entry 'panjang' value
                System.out.print("Masukan lebar (cm): ");
                float lebar = scanner.nextFloat(); // input to entry 'lebar' value
                System.out.println(line);
                System.out.println(""); // make empty row
                System.out.println("Luas persegi panjang dengan panjang " + panjang + "cm dan lebar " + lebar
                        + "cm = " + luasPersegiPanjang(panjang, lebar) + "cm^2"); // result
                System.out.println(""); // make empty row
                // post-claculation menu
                System.out.println("1. Kembali ke menu utama");
                System.out.println("0. Tutup Aplikasi");
                System.out.print("Input : ");
                int next4 = scanner.nextInt(); // input of post-calculation menu
                switch (next4) {
                    case 1:
                        mainMenu(); // redirect to main menu
                        break;
                    case 0:
                        closeApp = 1; // make a condition to stop the loop
                        break;
                }
                System.out.println(line);
                break;
        }
    }

    /***
     * Create menus to display when user select one of the type of 'bangun ruang'
     * 
     * @param input
     */
    private static void volumeBangunRuang(int input) {
        switch (input) {
            /***
             * Create case to display when 'Kubus' selected
             */
            case 1:
                clearScreen(); // clear console
                System.out.println(line);
                System.out.println("Hitung volume kubus"); // tittle
                System.out.println(line);
                System.out.print("Masukan sisi kubus (cm): ");
                float sisi = scanner.nextFloat(); // input to entry 'sisi' value
                System.out.println(line);
                System.out.println(""); // make empty row
                System.out.println("Volume kubus dengan sisi " + sisi + "cm = " + volumeKubus(sisi) + "cm^3"); // result
                System.out.println(""); // make empty row
                // post-calculation menu
                System.out.println("1. Kembali ke menu utama");
                System.out.println("0. Tutup Aplikasi");
                System.out.print("Input : ");
                int next1 = scanner.nextInt(); // input of post-calculation menu
                switch (next1) {
                    case 1:
                        mainMenu(); // redirect to main menu
                        break;
                    case 0:
                        closeApp = 1; // make a condition
                        break;
                }
                System.out.println(line);
                break;
            /***
             * Create case to display when 'Balok' selected
             */
            case 2:
                clearScreen(); // clear console
                System.out.println(line);
                System.out.println("Hitung volume balok"); // tittle
                System.out.println(line);
                System.out.print("Masukan panjang (cm): ");
                float panjang = scanner.nextFloat(); // input to entry 'panjang' value
                System.out.print("Masukan lebar (cm): ");
                float lebar = scanner.nextFloat(); // input to entry 'lebar' value
                System.out.print("Masukan tinggi (cm): ");
                float tinggi = scanner.nextFloat(); // input to entry 'tinggi' value
                System.out.println(line);
                System.out.println(""); // make empty row
                System.out.println("Volume balok dengan panjang " + panjang + "cm, lebar " + lebar + "cm dan tinggi "
                        + tinggi + "cm = " + volumeBalok(panjang, lebar, tinggi) + "cm^3"); // result
                System.out.println(""); // make empty row
                // post-calculation menu
                System.out.println("1. Kembali ke menu utama");
                System.out.println("0. Tutup Aplikasi");
                System.out.print("Input : ");
                int next2 = scanner.nextInt(); // input of post-calculation menu
                switch (next2) {
                    case 1:
                        mainMenu(); // redirect to main menu
                        break;
                    case 0:
                        closeApp = 1; // make a condition to stop the loop
                        break;
                }
                System.out.println(line);
                break;
            /***
             * Create case to display when 'Tabung' selected
             */
            case 3:
                clearScreen(); // clear console
                System.out.println(line);
                System.out.println("Hitung volume tabung"); // tittle
                System.out.println(line);
                System.out.print("Masukan jari-jari (cm): ");
                float jarijari = scanner.nextFloat(); // input to entry 'jarijari' value
                System.out.print("Masukan tinggi (cm): ");
                float tinggiTabung = scanner.nextFloat(); // input to entry 'tinggiTabung' value
                System.out.println(line);
                System.out.println(""); // make empty row
                System.out.println("Volume tabung dengan jari-jari " + jarijari + "cm dan tinggi " + tinggiTabung
                        + "cm = " + volumeTabung(jarijari, tinggiTabung) + "cm^3"); // result
                System.out.println(""); // make empty row
                // post-calculation menu
                System.out.println("1. Kembali ke menu utama");
                System.out.println("0. Tutup Aplikasi");
                System.out.print("Input : ");
                int next3 = scanner.nextInt(); // input of post-calculation menu
                switch (next3) {
                    case 1:
                        mainMenu(); // redirect to main menu
                        break;
                    case 0:
                        closeApp = 1; // make a condition to stop the loop
                        break;
                }
                System.out.println(line);
                break;
        }
    }

    /***
     * Create method to count 'luas persegi'
     */
    private static float luasPersegi(float sisi) {
        float res = sisi * sisi; // 'luas persegi' operation
        return res;
    }

    /***
     * Create method to count 'luas lingkaran'
     * 
     * @param jarijari
     */
    private static float luasLingkaran(float jarijari) {
        float res = (float) (phi * (jarijari * jarijari)); // 'luas lingkaran' operation
        return res;
    }

    /****
     * Create method to count 'luas segitiga'
     * 
     * @param alas
     * @param tinggi
     * @return
     */
    private static float luasSegitiga(float alas, float tinggi) {
        float res = (float) (0.5 * alas * tinggi); // 'luas segitiga' operation
        return res;
    }

    /***
     * Create method to count 'persegi panjang'
     * 
     * @param panjang
     * @param lebar
     * @return
     */
    private static float luasPersegiPanjang(float panjang, float lebar) {
        float res = (panjang * lebar); // 'luas persegi panjang' operation
        return res;
    }

    /***
     * Create method to count 'volume kubus'
     * 
     * @param sisi
     * @return
     */
    private static float volumeKubus(float sisi) {
        float res = sisi * sisi * sisi; // 'volume kubus' operation
        return res;
    }

    /***
     * Create method to count 'volume balok'
     * 
     * @param panjang
     * @param lebar
     * @param tinggi
     * @return
     */
    private static float volumeBalok(float panjang, float lebar, float tinggi) {
        float res = panjang * lebar * tinggi; // 'volume balok' operation
        return res;
    }

    /***
     * Create method to count 'volume tabung'
     * 
     * @param jarijari
     * @param tinggiTabung
     * @return
     */
    private static float volumeTabung(float jarijari, float tinggiTabung) {
        float res = (float) phi * (jarijari * jarijari) * tinggiTabung; // 'volume tabung' operation
        return res;
    }

    /***
     * Create main method
     * 
     * @param args
     * @throws InterruptedException
     */
    public static void main(String[] args) throws InterruptedException {
        /***
         * Make a loop for the app to run until we close/stop it
         */
        while (closeApp != 1) {
            mainMenu(); // display main menu
            System.out.print("Input : ");
            int input1 = scanner.nextInt(); // input to select choices from main menu
            /***
             * Create condition if 'Hitung luas bidang datar' is selected
             */
            if (input1 == 1) {
                menuBidang();
                System.out.print("Input : ");
                int input2 = scanner.nextInt();
                luasBidang(input2);
            }
            /***
             * Create condition if 'Hitung volume bangun ruang' is selected
             */
            else if (input1 == 2) {
                menuBangunRuang();
                System.out.print("Input : ");
                int input3 = scanner.nextInt();
                volumeBangunRuang(input3);
            } else if (input1 == 0) {
                /***
                 * Create condition if 'Tutup aplikasi' is selected
                 */
                closeApp = 1;
            } else {
                clearScreen();
                System.out.println(line);
                System.out.println("");
                System.out.println("Input tidak valid");
                System.out.println("");
                System.out.println(line);
                Thread.sleep(1000);
                mainMenu();
            }
        }
    }
}
